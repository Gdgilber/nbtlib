use std::io::Read;
use std::io::{Error, ErrorKind};
use std::string;

use byteorder::{BigEndian, ReadBytesExt};

#[derive(Debug)]
pub enum Tag {
    End,
    Byte(i8),
    Short(i16),
    Int(i32),
    Long(i64),
    Float(f32),
    Double(f64),
    ByteArray(Vec<u8>),
    String(string::String),
    List(Vec<Node>),
    Compound(Vec<Node>),

    // Newer method, unused probably
    //IntArray(Vec<i32>),
}

#[derive(Debug)]
pub struct Node {
    pub name: String,
    pub value: Tag,
}

use std::io::Result;

impl Node {
    pub fn new(read: &mut Read) -> Result<Node> {
        Self::read_node(read)
    }

    fn read_node(read: &mut Read) -> Result<Node> {
        let tag = read.read_i8();
        tag.and_then(|t| {
            Self::read_node_with_tag(read, t)
        })
    }

    fn read_node_with_tag(read: &mut Read, tag: i8) -> Result<Node> {
        let name = match tag {
            0 => Ok(String::new()),
            _ => Self::read_string(read),
        };
        name.and_then(|n| {  
            let val = Self::read_tag_value(read, tag);
            val.and_then(|v| {
                Ok(Node {
                    name: n,
                    value: v,
                })
            })
        })
    }

    fn read_tag_value(read: &mut Read, tag: i8) -> Result<Tag> {
        match tag {
            0 => Ok(Tag::End),
            1 => Self::read_byte_tag(read),
            2 => Self::read_short_tag(read),
            3 => Self::read_int_tag(read),
            4 => Self::read_long_tag(read),
            5 => Self::read_float_tag(read),
            6 => Self::read_double_tag(read),
            7 => Self::read_byte_array_tag(read),
            8 => Self::read_string_tag(read),
            9 => Self::read_list_tag(read),
            10 => Self::read_compound_tag(read),
            x => Err(Error::new(ErrorKind::Other, format!("Read bad tag: {}", x))),
        }
    }

    fn read_byte_tag(read: &mut Read) -> Result<Tag> {
        let byte = read.read_i8();
        byte.and_then(|b| {
            Ok(Tag::Byte(b))
        })
    }

    fn read_short_tag(read: &mut Read) -> Result<Tag> {
        let short = read.read_i16::<BigEndian>();
        short.and_then(|s| {
            Ok(Tag::Short(s))
        })
    }
    fn read_int_tag(read: &mut Read) -> Result<Tag> {
        let int = read.read_i32::<BigEndian>();
        int.and_then(|i| {
            Ok(Tag::Int(i))
        })
    }
    fn read_long_tag(read: &mut Read) -> Result<Tag> {
        let long = read.read_i64::<BigEndian>();
        long.and_then(|l| {
            Ok(Tag::Long(l))
        })
    }
    fn read_float_tag(read: &mut Read) -> Result<Tag> {
        let float = read.read_f32::<BigEndian>();
        float.and_then(|f| {
            Ok(Tag::Float(f))
        })
    }
    fn read_double_tag(read: &mut Read) -> Result<Tag> {
        let double = read.read_f64::<BigEndian>();
        double.and_then(|d| {
            Ok(Tag::Double(d))
        })
    }
    fn read_byte_array_tag(read: &mut Read) -> Result<Tag> {
        let len = read.read_i32::<BigEndian>();
        len.and_then(|l| {
            let mut vec = Vec::new();
            let result = read.take(l as u64).read_to_end(&mut vec);
            result.and_then(|_| {
                Ok(Tag::ByteArray(vec))
            })
        })
    }
    fn read_string_tag(read: &mut Read) -> Result<Tag> {
        let value = Self::read_string(read);
        value.and_then(|v| {
            Ok(Tag::String(v))
        })
    }
    fn read_list(read: &mut Read, count: i32, tag: i8) -> Result<Vec<Node>> {
        let mut nodes = Vec::new();
        for _ in 0..count {
            let tag = Self::read_tag_value(read, tag);
            match tag {
                Ok(t) => nodes.push(Node{name:String::new(), value:t}),
                Err(e) => return Err(e),
            }
        }
        Ok(nodes)
    }
    fn read_list_tag(read: &mut Read) -> Result<Tag> {
        let tag = read.read_i8();
        tag.and_then(|t| {
            let len = read.read_i32::<BigEndian>();
            len.and_then(|l| {
                let list = Self::read_list(read, l, t);
                list.and_then(|li| {
                    Ok(Tag::List(li))
                })
            })
        })
    }

    fn read_compound(read: &mut Read) -> Result<Vec<Node>> {
        let mut nodes = Vec::new();
        loop {
            let node = Self::read_node(read);
            match node {
                Ok(Node{name:_,value:Tag::End}) => {
                    break;
                },
                Ok(n) => nodes.push(n),
                Err(e) => {
                    return Err(e);
                },
            }
        }
        Ok(nodes)
    }

    fn read_compound_tag(read: &mut Read) -> Result<Tag> {
        let compound = Self::read_compound(read);
        compound.and_then(|c| {
            Ok(Tag::Compound(c))
        })
    }

    fn read_string(read: &mut Read) -> Result<String> {
        let len = read.read_i16::<BigEndian>();
        len.and_then(|l| {
            let mut string = String::new();
            let result = read.take(l as u64).read_to_string(&mut string);
            result.and(Ok(string))
        })
    }
}
