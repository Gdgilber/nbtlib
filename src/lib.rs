extern crate byteorder;
extern crate flate2;


mod nbt;

use nbt::Node;
use std::fs::File;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Read;

use byteorder::{ReadBytesExt};
use flate2::read::GzDecoder;

pub fn read_nbt(filename: &str) -> std::io::Result<Node> {
    let file = File::open(filename);
    file.and_then(|mut file| {
        let magic = file.read_u8();
        file.seek(SeekFrom::Start(0)).unwrap();
        match magic {
            Ok(0x1f) => read_gzipped_nbt(&mut file),
            Ok(0x10) => Node::new(&mut file),
            Ok(val) => panic!("Invalid magic number: {}", val),
            Err(e) => Err(e),
        }
    })
}

fn read_gzipped_nbt(read: &mut Read) -> std::io::Result<Node> {
    let decoder = GzDecoder::new(read);
    decoder.and_then(|mut d| Node::new(&mut d))
}

#[cfg(test)]
mod tests {
    use nbt::{Tag, Node};
    #[test]
    fn test_deep_file() {
        let mut file: Vec<u8> = Vec::new();
        for _ in 0..512 {
            //Compound with name == ""
            file.push(0x0a);
            file.push(0x00);
            file.push(0x00);
        }
        for _ in 0..512 {
            //End for each compound
            file.push(0x00);
        }
        let mut slice: &[u8] = &file;
        let node = Node::new(&mut slice).unwrap();
        match node.value{
            Tag::Compound(vec) => assert_eq!(vec.len(), 1),
            _ => panic!("bad base tag type"),
        }
    }
    #[test]
    fn test_big_file() {
        let node = ::read_nbt("bigtest.nbt").unwrap();
        assert_eq!(node.name, "Level");
        match node.value {
            Tag::Compound(vec) => {
                assert_eq!(vec[0].name, "longTest");
                assert_eq!(vec[1].name, "shortTest");
                assert_eq!(vec[2].name, "stringTest");
                assert_eq!(vec[3].name, "floatTest");
                assert_eq!(vec[4].name, "intTest");
                assert_eq!(vec[5].name, "nested compound test");
                assert_eq!(vec[6].name, "listTest (long)");
                assert_eq!(vec[7].name, "listTest (compound)");
                assert_eq!(vec[8].name, "byteTest");
                assert_eq!(vec[9].name, "byteArrayTest (the first 1000 values of (n*n*255+n*7)%100, starting with n=0 (0, 62, 34, 16, 8, ...))");
                assert_eq!(vec[10].name, "doubleTest");

            }
            _ => {
                panic!("Bad type of base tag");
            }
        }
    }
}
